import { v4 as uuidv4 } from 'uuid';


const getEntropy = roomName => {
    if (typeof Storage === 'undefined') {
        return uuidv4();
    }

    const prevValue = sessionStorage.getItem(roomName);

    if (prevValue) {
        return prevValue;
    }

    const newValue = uuidv4();

    sessionStorage.setItem(roomName, newValue);

    return newValue;

    // return '123';
};

export const generateVisitorUrlParams = roomName => `?room=${roomName}&entropy=${getEntropy()}&height=${
    window.screen.height
}&width=${window.screen.width}`;
