export const fetchRoomTitle = async roomName => {
    try {
        const roomId = roomName.replace(/\s/g, '').toLowerCase();
        const roomInfo = await $.getJSON(`https://api.hamoonja.com/meetings/${roomId}`);

        return roomInfo.title;
    } catch (error) {
        return null;
    }
};

