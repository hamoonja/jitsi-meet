import React from 'react';

import AppSuggestion from './components/app-suggestion';
import BrowserSuggestion from './components/browser-suggestion';

const supportedGuide = `
در حال وصل شدن به جلسه
`;

const MobileExperienceManager = ({
    Platform,
    downloadAppUrl,
    isBrowserSupported = true,
    onAppDownload,
    onDeepLink,
    openInAppUrl
}) => {

    if (isBrowserSupported) {
        return <div className="full-page-centered">{supportedGuide}</div>;
    }

    return <div className="full-page-centered">
        <BrowserSuggestion os = {Platform.OS} />
        <AppSuggestion
            downloadAppUrl = { downloadAppUrl }
            onAppDownload = { onAppDownload }
            onDeepLink = { onDeepLink }
            openInAppUrl = { openInAppUrl }
        />
    </div>;
};


export default MobileExperienceManager;
