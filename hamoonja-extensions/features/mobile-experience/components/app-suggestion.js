import React from 'react';

const openInAppText = `
در نرم‌افزار موبایل
Jitsi
این جلسه را دنبال کنید:
`;

const installAppText = `
یا اگر آن را نصب نکرده‌اید نصب کنید:
`;

const openButtonText = `
باز کردن در Jitsi
`;

const installButtonText = `
نصب کردن Jitsi
`;

const AppSuggestion = ({
    downloadAppUrl,
    onAppDownload,
    onDeepLink,
    openInAppUrl
}) => <div className="app-suggestion-container">
    <p>{openInAppText}</p>
    <a className="button" href = { openInAppUrl } onClick = { onDeepLink } >{openButtonText}</a>
    <br />
    <p>{installAppText}</p>
    <a className="button" href = { downloadAppUrl } onClick = { onAppDownload } >{installButtonText}</a>
</div>;

export default AppSuggestion;
