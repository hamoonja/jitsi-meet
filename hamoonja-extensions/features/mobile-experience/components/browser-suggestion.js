import React from 'react';

const copyUrlText = `
کپی کردن نشانی اتاق جلسه
`;

const openInBrowserText = `
برای ورود به جلسه در این مرورگر نشانی زیر را کپی کنید و در مرورگر مقصد وارد کنید:
`;

const guideText = os => {
    const browser = os === 'ios' ? 'Safari' : 'Google Chrome';

    if (!(os === 'android' || os === 'ios')) {
        return null;
    }

    return `
    برای حضور در این جلسه باید از مرورگر
    ${browser}

    یا نرم افزار موبایل
    Jitsi
    استفاده کنید.
    `;
};

const doNothing = () => {
    return {};
};

const renderAlertText = () => `
نشانی جلسه در کلیپ‌بورد ذخیره شد.
آن را در مرورگر مقصد وارد کنید.
`;


const BrowserSuggestion = ({ os }) => {
    const copyToClipboard = React.useCallback(() => {
        const targetElem = document.getElementById('meeting-url');

        targetElem.select();
        targetElem.setSelectionRange(0, 99999);
        document.execCommand('copy');
        // eslint-disable-next-line no-alert
        alert(renderAlertText(targetElem.value));
    }, []);

    return <div className="browswer-suggestion-container">
        <p className="rtl-text">{guideText(os)}</p>
        <p>{ openInBrowserText }</p>
        <input id="meeting-url" value={window.location.href} type="text" onChange={doNothing}/>
        <button type="button" className="copy-button button" onClick={copyToClipboard}>{copyUrlText}</button>
    </div>;
};

export default BrowserSuggestion;
