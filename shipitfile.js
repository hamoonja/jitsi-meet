const appName = "hamoonja-jitsi-front";

const finalDeployAddress =
    "/home/easyadmin/code/js/hamoonja-jitsi-meet/current";
const deployToAddress =
    "/home/easyadmin/code/js/hamoonja-jitsi-meet/shipitfiles";
const gitRepo = "https://gitlab.com/hamoonja/jitsi-meet.git";
const serverSSHaddress = "easyadmin@94.182.190.182";
const packageDir = "source_package/jitsi-meet";

// const workspace = './shipit-workspace';

module.exports = (shipit) => {
    require("shipit-deploy")(shipit);

    const path = require("path");

    shipit.initConfig({
        default: {
            deployTo: deployToAddress,
            repositoryUrl: gitRepo,
            keepReleases: 5,
            dirToCopy: packageDir,
        },
        production: {
            servers: serverSSHaddress,
        },
    });

    // const ecosystemFilePath = path.join(
    //     shipit.config.deployTo,
    //     ecosystemFileName
    // );

    shipit.on("fetched", () => {
        shipit.start("build");
    });

    shipit.on("published", () => {
        shipit.start("copy-to-final-destination");
    });

    shipit.blTask("build", async () => {
        await shipit.local(
            `cd ${shipit.workspace} && npm install && npm run build`
        );
        shipit.emit("built");
    });

    shipit.blTask("copy-to-final-destination", async () => {
        await shipit.remote(`rm -r ${finalDeployAddress}/*`);
        await shipit.remote(
            `cp -a ${deployToAddress}/current/. ${finalDeployAddress}`
        );

        // await shipit.remote("~/docker/runnginx.sh");
        // await shipit.remote(
        //     "docker exec -t nginx-main sh -c 'nginx -s reload'"
        // );
    });
};
